﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV_7_Analysis
{
    public partial class formNewGame : Form
    {
        formTicTacToe TicTacToe;
        public formNewGame(formTicTacToe parent)
        {
            InitializeComponent();
            TicTacToe = parent;
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            if (tbP1Name.Text == "" || tbP2Name.Text == "") MessageBox.Show("Please input player names.", "Error");
            else
            {
                TicTacToe.lblP1Score.Text = "0";
                TicTacToe.lblP2Score.Text = "0";
                TicTacToe.bNewGame.Enabled = true;
                TicTacToe.lblP1Name.Text = tbP1Name.Text.ToString();
                TicTacToe.lblP2Name.Text = tbP2Name.Text.ToString();
                TicTacToe.pictureBox1.Enabled = true;
                TicTacToe.pictureBox1.ImageLocation = "";
                TicTacToe.pictureBox2.Enabled = true;
                TicTacToe.pictureBox2.ImageLocation = "";
                TicTacToe.pictureBox3.Enabled = true;
                TicTacToe.pictureBox3.ImageLocation = "";
                TicTacToe.pictureBox4.Enabled = true;
                TicTacToe.pictureBox4.ImageLocation = "";
                TicTacToe.pictureBox5.Enabled = true;
                TicTacToe.pictureBox5.ImageLocation = "";
                TicTacToe.pictureBox6.Enabled = true;
                TicTacToe.pictureBox6.ImageLocation = "";
                TicTacToe.pictureBox7.Enabled = true;
                TicTacToe.pictureBox7.ImageLocation = "";
                TicTacToe.pictureBox8.Enabled = true;
                TicTacToe.pictureBox8.ImageLocation = "";
                TicTacToe.pictureBox9.Enabled = true;
                TicTacToe.pictureBox9.ImageLocation = "";
                TicTacToe.lblTurn.Text = tbP1Name.Text.ToString() + "'s turn";
                Close();
            }
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            TicTacToe.bNewGame.Enabled = true;
        }
    }
}
